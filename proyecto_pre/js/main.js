//Funciones propias
import { setRRSSLinks, scrollActiv } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';

//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Vars
let viewportHeight = window.innerHeight;
let logoFooter = document.getElementById("logoFooter");
let sharing = document.getElementById("sharing");
let summaries = document.getElementsByClassName("summary");

//Ejecución
main();

function main() {
	setRRSSLinks();
}

window.addEventListener('scroll', function(){
	scrollActiv();
	
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}

	if (isMobile()) {
		if (isElementInViewport(logoFooter)) {
			sharing.classList.remove("visible")
		}
	}

	for (let i = 0; i < summaries.length; i++) {
		if(isElementInViewport(summaries[i])) {
			if(!summaries[i].classList.contains('summary--animated')) {
				summaries[i].classList.add('summary--animated')
			}
		}
	}
});