let scroll25 = false;
let scroll50 = false;
let scroll75 = false;
let scroll100 = false;
let scrollPercent;

function scrollActiv(){
	scrollPercent = getScrollPercent();

	if (typeof universalGa !== 'undefined') {
		if ( scrollPercent >= 25 ) {
			if ( !scroll25 ) {
				universalGa('t1.send', 'event', 'Ruina y olvido para uno de los gulags más terribles de la posguerra española', 'scroll', 'Scroll Depth 25%');
				scroll25 = true;
			}
		}
		if ( scrollPercent >= 50 ) {
			if ( !scroll50 ) {
				universalGa('t1.send', 'event', 'Ruina y olvido para uno de los gulags más terribles de la posguerra española', 'scroll', 'Scroll Depth 50%');
				scroll50 = true;
			}
		}
		if ( scrollPercent >= 75 ) {
			if ( !scroll75 ) {
				universalGa('t1.send', 'event', 'Ruina y olvido para uno de los gulags más terribles de la posguerra española', 'scroll', 'Scroll Depth 75%');
				scroll75 = true;
			}
		}

		if ( scrollPercent >= 100 ) {
			if ( !scroll100 ) {
				universalGa('t1.send', 'event', 'Ruina y olvido para uno de los gulags más terribles de la posguerra española', 'scroll', 'Scroll Depth 100%');
				scroll100 = true;
			}
		}
	}

}

function getScrollPercent() {

	let height = document.documentElement.clientHeight;
	let scrollHeight = document.documentElement.scrollHeight - height;
    let scrollTop = document.documentElement.scrollTop;
    let percent = Math.round(scrollTop / scrollHeight * 100);

	return percent;
}

function setRRSSLinks() {
    let urlPage = window.location.href;

    //Facebook
    let shareFB = document.getElementById("shareFB")
    let fbHref = "https://www.facebook.com/sharer/sharer.php?u="+urlPage
    shareFB.setAttribute("href",fbHref)

    //Twitter
    let shareTW = document.getElementById("shareTW")
    let twText = shareTW.getAttribute("data-text")
    let twHref = "https://twitter.com/intent/tweet?url="+urlPage+"&text="+twText+"&original_referer="+urlPage
    shareTW.setAttribute("href",twHref)

    //Linkedin
    let shareLK = document.getElementById("shareLK")
    let lkText = shareLK.getAttribute("data-text")
    let lkHref = "https://www.linkedin.com/shareArticle?mini=true&url="+urlPage+"&title="+lkText+"&summary=&source="
    shareLK.setAttribute("href",lkHref)

    //WhatsApp
    let shareWA = document.getElementById("shareWA")
    let waText = shareWA.getAttribute("data-text")
    let waHref = "https://api.whatsapp.com/send?text="+waText+" "+urlPage
    shareWA.setAttribute("href",waHref)
}

export {scrollActiv, setRRSSLinks}